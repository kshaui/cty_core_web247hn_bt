


$(document).ready(function() {
    openPopup('.buy-now', '.popup');
    closePopup('.popup_close', '.popup'); 
})
function openPopup(clickShowBtn, popupShow) {
    $(clickShowBtn).on('click',function(){
        $(popupShow).bPopup({
            speed: 450,
            transition: 'slideDown',
            zIndex:99999,
            onOpen: function() { 
                $(popupShow).css('visibility', 'visible'); 
            },
            onClose: function() { 
                $(popupShow).css('visibility', 'hidden'); 
            }
        });
    }); 
}
function closePopup(clickCloseBtn, popupClose) {
    $(clickCloseBtn).on('click' ,function() {
        $(popupClose).css('visibility', 'hidden');
        $(popupClose).bPopup().close();
    })
}
