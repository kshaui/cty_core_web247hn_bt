<?php 
$test = 'Mot so vi du de tim hieu ham string trong PHP';
echo strlen($test)." <br>";
echo str_word_count($test)."<br>";
echo str_replace(" string"," array",$test)."<br>";
strtoupper($test)."<br>";
strtolower($test)."<br>";

print_r(str_word_count($test,1))."<br>";
echo substr($test,27)."<br>";
echo $test.' Basic'." <br>";
echo md5($test)."<br>";
echo md5($test, false)."<br>";
//hiển thị các kí tự html
$test1="<html> </html>";
$test2 = htmlentities( $test1, ENT_COMPAT, 'UTF-8')."<br >";
$htmlentity = htmlentities( $test2, ENT_COMPAT, 'UTF-8');

echo $test2."<br>";
echo $htmlentity."<br>";
$a = '&lt;a href=&quot;https://www.w3schools.com&quot;&gt;w3schools.com&lt;/a&gt;';
echo html_entity_decode($a)."<br>";
//loại bỏ các kí tự html
$b = 'Đây là <b> đoạn mã</b> <a href="#">cần loại bỏ</a>
<p>các thẻ html</p>';
echo $b;
echo strip_tags($b) . "<br>";
// hàm json_decode() chuyển một chuỗi json về dạng mảng hoặc dạng object
$json_string='
{
	"a":"name",
	"b":"email",
	"c":"website"
}';
var_dump(json_decode($json_string,true));
var_dump(json_encode($json_string));

// hàm json_encode() chuyển 1 mảng hoặc một object về dạng chuỗi
$array=array(
	"a"=>"name",
	"b"=>"email",
	"c"=>"website"
);
echo json_encode($array);

$c=array(28,"Hello", 05, "world", 2018, "PHP", "Hello", 05);
 echo count($c)."<br>";
 echo '<pre>';
 print_r(array_count_values ( $c ))."<br>";
 echo '</pre>';
 //xóa phần tử đầu tiên
 array_pop($c);
 echo '<pre>';
 print_r($c)."<br>";
 echo '</pre>';
 //xóa phần tử cuỗi cùng
 array_shift($c);
 echo '<pre>';
 print_r($c)."<br>";
 echo '</pre>';
 //xóa phần tử thứ 2,5
unset($c[2], $c[5]);
echo "<pre>";
    print_r($c);
echo "</pre>";
//xóa phần tử trùng nhau
echo "<pre>";
    print_r(array_unique($c, 0));
echo "</pre>";
//kiểm tra xem có tồn tại phần tử html ko
print_r(in_array('HTML', $c));
print_r(in_array('PHP', $c));
//kiểm tra xem có phải mảng hay ko
    print_r(is_array($array));
//chuyển đổi mảng về chuỗi
 echo implode(" ", $c)."<br>";
//
$c=array(28,"Hello",05,"world",2018,"PHP","Hello",05);
echo "<pre>";
    print_r(array_slice ($c, 4.6));
echo "</pre>";
echo array_search('2018',$c)."<br>"; 
echo "<pre>";
    print_r(array_reverse ($c));
echo "</pre>";
array_push($c,"html","css","js");
echo "<pre>";
    print_r($c);
echo "</pre>";
//bai4
$d=array(4,1,-9,7,2,9,0,-4);
sort($d);
//tong giá trị phần tử trong mảng
$tong=0;
$n=count($d);
for ($i = 0; $i <$n ; $i++) {
	$tong+=$d[$i];	
}
echo $tong."<br>";
//sx tăng dần
echo 'Sắp xếp tăng dần <br>';
for ($i=0; $i <$n-1 ; $i++) {
	if($d[$i]>$d[$i+1]){ 
		$temp=$d[$i];
		$d[$i]=$d[$i+1];
		$d[$i+1]=$temp;			
	}
}
	echo "<pre>";
	print_r($d);
	echo "</pre>";
echo 'Sắp xếp giảm dần <br>';
$d=array(4,1,-9,7,2,9,0,-4);
for($i=1; $i<$n; $i++)
   for( $j=$n-1; $j>=$i; $j--)
       if( $d[$j]>$d[$j-1])
           {
               $temp = $d[$j];
               $d[$j] = $d[$j-1];
               $d[$j-1]=$temp;
           }
echo "<pre>";
print_r($d);
echo "</pre>";
echo 'Các phần tử lớn hơn 5 <br>';	
for ($i = 0; $i <$n ; $i++) {
	if ($d[$i]>5) {
		echo  $d[$i]."<br>";		
	}
}
echo 'số chẵn <br>';
for ($i = 0; $i <$n ; $i++) {
	if ($d[$i]%2==0) {
		echo  $d[$i]."<br>";		
	}	
}
echo "số lẻ<br>";
for ($i = 0; $i <$n ; $i++){
	if ($d[$i]%2!=0) {
		echo  $d[$i]."<br>";		
	}
}
echo 'Tổng số phần tử âm, tổng phần tử âm <br>';
$dem=0;
$tong_am=0;
for ($i = 0; $i <$n ; $i++) {
	if ($d[$i]<0) {
		$dem++;
		$tong_am+=$d[$i];
		$d[$i]=$d[$i+1];
		$n--;
		echo  $d[$i]."<br>";		
	}	
}
echo $dem."<br>";
echo $tong_am."<br>";
for ($i = 0; $i <$n ; $i++) {
	if ($d[$i]>0) {
		echo  $d[$i]."<br>";
		
	}	
}

//các hàm xử lý thời gian
//Timezone Việt nam
date_default_timezone_set('Asia/Ho_Chi_Minh');
echo date('d/m/Y - H:i:s')."<br>";//trả về ngày số, tháng số ,năm đầy đủ 4 số
echo date('\B\â\y \g\i\ờ \l\à H \g\i\ờ')."<br>";
//Lưu định dạng ngày tháng trong mySQL
echo date('Y-m-d H:i:s')."<br>";
//Hàm mktime() tính toán trả về ngày chính xác nhưng ở int, dùng date()
$dateint = mktime(0, 0, 0, 11, (34 + 22), 2018);
echo date('d/m/Y', $dateint)."<br>";
//Xem được ngày tới tháng tới, năm tới
$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
$lastmonth = mktime(0, 0, 0, date("m")-7, date("d"),   date("Y"));
$nextyear  = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+3);

 ?>
