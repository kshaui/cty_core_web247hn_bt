<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\ProductCategory;
use App\Filter;
use App\FilterDetail;
class ProductController extends Controller
{
    public function AllShow($filters = null)
    {
        $paginate = 20;
        $data = Product::where('status',1);
        $product_categories = ProductCategory::where('status',1)->get();

        /**
         * bộ lọc động
         */
        if($filters != null)
        {
            $filter_detail_f = FilterDetail::where('status',1)
                ->where('slug',$filters)
                ->first() ;
            dump($filter_detail_f);
            /**
             * lấy ra id của products
             */
            $product_filter = DB::table('product_filters')
                ->where('filter_id',$filter_detail_f->id)
                ->pluck('product_id','product_id')->toArray() ;  
            
                dump($product_filter);

            $data = $data->whereIn('id',$product_filter);
        }
        /**
         * lấy ra bộ lọc đã chọn của sản phẩm
         */
        $product_filters_id =collect( DB::table('product_filters')
        ->leftJoin('filter_details', 'product_filters.filter_id','=','filter_details.id')
        ->get() );        
        /**
         * danh mục bộ lọc đổ ra
         */
        $filter_cate = Filter::where('status',1)
            ->whereIn('id',$product_filters_id->unique('filter_id')->pluck('filter_id','filter_id')->toArray())
            ->get(); 
        /**
         * chi tiết bộ lọc đổ ra 
         */
        $filter_details_collect =collect( FilterDetail::where('status',1)
            ->whereIn('filter_id',$filter_cate->pluck('id','id')->toArray())
            ->whereIn('id',$product_filters_id->pluck('id','id')->toArray())
            ->get() );
        
        /**
         * end bộ lọc
         */
      

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => 'Sản phẩm',
            'description'=> 'Sản phẩm',
            'url' => url('').'/san-pham',
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],         
        ];
        $data = $data->paginate($paginate);
      
        return view('mobile.products.index',compact('data','meta_seo','breadcrumbs','product_categories'));
    }
    public function index($slug)
    { 
        $paginate = 20;
        $category = ProductCategory::where('status',1)->where('slug',$slug)->firstOrFail();
        $product_categories = ProductCategory::where('status',1)->get();
        $data = Product::where('status',1)->where('category_id',$category->id); 


         /**
         * bộ lọc động
         */
        if($filters != null)
        {
            $filter_detail_f = FilterDetail::where('status',1)
                ->where('slug',$filters)
                ->first() ;
            dump($filter_detail_f);
            /**
             * lấy ra id của products
             */
            $product_filter = DB::table('product_filters')
                ->where('filter_id',$filter_detail_f->id)
                ->pluck('product_id','product_id')->toArray() ;  
            
                dump($product_filter);

            $data = $data->whereIn('id',$product_filter);
        }
        /**
         * lấy ra bộ lọc đã chọn của sản phẩm
         */
        $product_filters_id =collect( DB::table('product_filters')
        ->leftJoin('filter_details', 'product_filters.filter_id','=','filter_details.id')
        ->get() );        
        /**
         * danh mục bộ lọc đổ ra
         */
        $filter_cate = Filter::where('status',1)
            ->whereIn('id',$product_filters_id->unique('filter_id')->pluck('filter_id','filter_id')->toArray())
            ->get(); 
        /**
         * chi tiết bộ lọc đổ ra 
         */
        $filter_details_collect =collect( FilterDetail::where('status',1)
            ->whereIn('filter_id',$filter_cate->pluck('id','id')->toArray())
            ->whereIn('id',$product_filters_id->pluck('id','id')->toArray())
            ->get() );
        
        /**
         * end bộ lọc
         */

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $category->getName(),
            'description'=> $category->getDes(160),
            'url' => $category->getUrl(),
            'image'=> $category->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],   
            ['name'=> $category->getName(),'url' =>  $category->getUrl()],         
        ];

        $data = $data->paginate($paginate);

        return view('mobile.products.index',compact('data','meta_seo','breadcrumbs','product_categories'));
    }
    public function show($slug)
    {
        $product = Product::where('status',1)->where('slug',$slug)->firstOrFail();

        if (isset($product->slides) && $product->slides != '') {
            $slides = explode(",",$product->slides);
        }else {
            $slides = '';
        }

        $product_category = ProductCategory::where('status',1)->where('id',$product->category_id)->firstOrFail();

        $promotion = preg_split('/\n|\r\n/',$product->promotion);

        
        
        if ($product->related_products != '') {
            //sản phẩm liên quan
            $related_product = $product->get_related_product();
        }

        // Nếu không có sp lq được chọn lấy auto 5 sp cùng danh mục
        if ($product->related_products == '') {
            $related_product = Product::where('status',1)->where('category_id',$product->category_id)->limit(5)->get();
        }

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $product->getName(),
            'description'=> $product->getDes(160),
            'url' => $product->getUrl(),
            'image'=> $product->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],   
            ['name'=> $product_category->getName(),'url' =>  $product_category->getUrl()],         
        ];

        return view('mobile.products.show',compact('product','meta_seo','breadcrumbs','product_category','related_product','slides'));
    }
}
