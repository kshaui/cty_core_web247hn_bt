<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;

use App\News;
use App\NewsCategory;

class NewsController extends Controller
{
    public function AllShow()
    {
        $data = News::where('status',1)->paginate(20);
        $news_categories = NewsCategory::where('status',1)->get();
        $meta_seo = $this->meta_seo('',0,
        [
            'title' => 'Tin tức',
            'description'=> 'Tin tức ',
            'url' => url('').'/tin-tuc',
        ]);
        $breadcrumbs = [
            ['name'=> 'Tin tức','url' => '/tin-tuc'],         
        ];
        return view('mobile.news.index',compact('data','meta_seo','breadcrumbs','news_categories'));
    }
    public function index($slug = null)
    {
        $category = NewsCategory::where('status',1)->where('slug',$slug)->firstOrFail();

        $news_categories = NewsCategory::where('status',1)->get();
        //nó
        dump($category);
        //cha nó
        dump($category->getParent());
        //các con nó
        dump($category->getChilds());
        
        $child_ids = $category->getChildIds();
        $data = News::where('status',1)->whereIn('category_id',$child_ids)->paginate(20);
        dump($data);

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $category->getName(),
            'description'=> $category->getDes(160),
            'url' => $category->getUrl(),
            'image'=> $category->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Tin tức','url' => '/tin-tuc'],   
            ['name'=> $category->getName(),'url' =>  $category->getUrl()],         
        ];

        return view('mobile.news.index',compact('data','meta_seo','breadcrumbs','news_categories'));
        
    }
    public function show($slug)
    {
        $news = News::where('status',1)->where('slug',$slug)->firstOrFail();
        $news_category = NewsCategory::where('status',1)->where('id',$news->category_id)->firstOrFail();

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $news->getName(),
            'description'=> $news->getDes(160),
            'url' => $news->getUrl(),
            'image'=> $news->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Tin tức','url' => '/tin-tuc'],   
            ['name'=> $news_category->getName(),'url' =>  $news_category->getUrl()],         
        ];

        return view('mobile.news.show',compact('news','meta_seo','breadcrumbs','news_category'));
    }
}
