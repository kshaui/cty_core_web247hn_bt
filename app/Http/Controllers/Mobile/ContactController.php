<?php
namespace App\Http\Controllers\Mobile;
use Illuminate\Http\Request;
class ContactController extends Controller
{
    public function index()
    {
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Liên hệ',
            'description'=> '',
        ]);
        $breadcrumbs = [
            ['name'=> 'Liên hệ','url' => '/lien-he'],               
        ];
        return view('mobile.contacts.index', compact('meta_seo','breadcrumbs'));
    }
}
