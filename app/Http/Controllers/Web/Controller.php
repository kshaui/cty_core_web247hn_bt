<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

use DB;
use View;
use Illuminate\Support\Facades\Cache;
class Controller extends BaseController
{
    public function __construct()
    {
        //constructor cho web
        $variable = 'variable';
        View::share('variable',$variable);
        
        Cache::flush();
        
        if(cache::has('config_general')) {
            $config_general = Cache::get('config_general');
        }else{
            $config_general = DB::table('options')->select('value')->where('name','general')->first();
            $config_general = json_decode(base64_decode($config_general->value),true);
            Cache::forever('config_general', $config_general);
        }

        if(cache::has('menu_primary_top')) {
            $menu_primary_top = Cache::get('menu_primary_top');
        }else {
            $menu_primary_top = $this->ChildrenCategoryOption('general', 'menu_primary');
            Cache::forever('menu_primary_top', $menu_primary_top);
             
        }
       

        if(cache::has('menu_footer')) {
            $menu_footer = Cache::get('menu_footer');
        }else {
            $menu_footer = $this->ChildrenCategoryOption('general', 'menu_footer');
            Cache::forever('menu_footer', $menu_footer);
             
        }


        View::share('config_general',$config_general);

        View::share('menu_primary_top', $menu_primary_top);
        View::share('menu_footer', $menu_footer);

    }

    /**
     * @param string $type - tên kiểu trong bảng meta_seo
     * @param int $id - id trong bảng meta_seo
     * @param array $options - các trường mặc định hoặc không có trong bảng meta_seo: title | description | robots | type | url | image |
     */
    public function meta_seo($type='',$id=0,$options) {
        $meta_seo = [];
        if(count($options)) {
            foreach ($options as $key=>$value) {
                $meta_seo[$key] = $value;
            }
        }
        if ($type != '' && $id != 0) {
            $data_seo = DB::table('meta_seo')->where('type',$type)->where('type_id',$id)->first();
            if($data_seo) {
                if($data_seo->title!=''){
                    $meta_seo['title'] = $data_seo->title;
                }
                if($data_seo->description!=''){
                    $meta_seo['description'] = $data_seo->description;
                }
                $meta_seo['robots'] = $data_seo->robots;
            }
        }
        return $meta_seo;
    }
    public function ChildrenCategoryOption($setting, $data)
    {
        $category = DB::table('options')->select('value')->where('name', $setting)->first();
        $category = json_decode(base64_decode($category->value), true);
        $data = json_decode($category[$data]);
        foreach ($data as $value) {
            if (!empty($value->children)) {
                $children = $value->children;
                $value->children = $children;
            }
        }
        return $data;
    }

    public function send_email($fun_mail)
    {
        $option = Cache::remember('setting_mail_configs', 300, function() {
            return DB::table('options')->select('value')->where('name','mail_configs')->first();
        });
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $config_general = $this->getOptions('general');
        config([
            'mail.driver'           => $data['driver']??config('mail.driver'),
            'mail.host'             => $data['host']??config('mail.host'),
            'mail.port'             => $data['port']??config('mail.port'),
            'mail.from.address'     => $data['from_address']??config('mail.from.address'),
            'mail.from.name'        => $data['from_name']??config('mail.from.name'),
            'mail.encryption'       => $data['encryption']??config('mail.encryption'),
            'mail.username'         => $data['username']??config('mail.username'),
            'mail.password'         => $data['password']??config('mail.password'),
            'mail.sendmail'         => $data['sendmail']??config('mail.sendmail')
        ]);
        $email = $config_general['email_order'];
        try {
            // TestEmail(['email'=>$email,'name'=>'sudo'.config('app.name')])
            Mail::to($email)->send($fun_mail);
            return [
                'status' => 1,
                'message' => 'Gửi thành công',
            ];
            // if (verify_email_org($email)) {
            // } return [
            //     'status' => 2,
            //     'message' => 'Email không tồn tại'
            // ];
        } catch (Exception $e) {
            return [
                'status' => 2,
                'message' => 'Gửi thất bại',
                'error' => $e->message(),
            ];
        }


    }

    public function getOptions($name) {
        $cache_name = 'options_'.$name;
        if(Cache::has($cache_name)) {
            return Cache::get($cache_name);
        }else {
            $config = DB::table('options')->select('value')->where('name',$name)->first();
            if($config) {
                $config = json_decode(base64_decode($config->value),true);
                Cache::forever($cache_name,$config);
                return $config;
            }
        }
        return;
    }
}
