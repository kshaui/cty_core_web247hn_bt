<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\MyClass\Categories;
use App\Product;
use App\ProductCategory;
use DB;
use Storage;
use Illuminate\Support\Facades\Cache;
class HomeController extends Controller
{
    public function index()
    {
        if (Cache::has('h_products')) {
            $h_products = Cache::get('h_products');
        } else {
            $h_products = Product::where('status',1)->leftJoin('pins', 'products.id','=','pins.type_id')->where('pins.type','products')->where('pins.place','home')->orderBy('value', 'asc')->select('products.*')->limit(20)->get();

            Cache::forever('h_products', $h_products);
        }

        if (Cache::has('h_slides')) {
            $h_slides = Cache::get('h_slides');
        } else {
            $h_slides = DB::table('slides')->get();
            Cache::forever('h_slides', $h_slides);
        }
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Trang chủ',
            'description'=> 'Mô tả trang chủ',
            'url' => url('/'),
            'image' => url('/').'/assets/img/logo.png'
        ]);
        $admin_bar_edit = route('admin.setting.home');
        return view('web.home',compact('meta_seo','admin_bar_edit','h_products','h_slides'));
    }

    //datafeeds tạo google shopping

    public function datafeeds()
    {
        $products = Product::where('status',1)->where('price','>',0)->whereNotNull('image')->whereNotNull('detail')->get();
     
        $data = "id\ttiêu đề\tmô tả\tliên kết\ttình trạng\tgiá\tcòn hàng\tliên kết hình ảnh\tnhãn hiệu\tdanh mục sản phẩm của Google";
        
        $setting_google_shopping = DB::table('options')->where('name','google_shopping')->first();
        $google_shopping = json_decode(base64_decode($setting_google_shopping->value));
       
        foreach($products as $value){
            $description = str_replace(['\n','\t'],'',cutString(removeHTML($value->detail),170));
        
            $url = $value->getUrl();
            $product_category = ProductCategory::where('status',1)->where('id',$value->category_id)->first();
            
            $price = $value->price;
            $google_shopping_product = DB::table('google_shopping')->where('type','products')->where('type_id',$value->id)->first();
          
            $google_shopping_brand = $google_shopping->brand;
            $google_shopping_category = $google_shopping->category;
            $google_shopping_instock = $google_shopping->instock;
            $google_shopping_itemcondition = $google_shopping->itemcondition;

            if(!empty($google_shopping_product)){
                if($google_shopping_product->brand != null){
                    $google_shopping_brand = $google_shopping_product->brand;
                }
                if($google_shopping_product->category != null){
                    $google_shopping_category = $google_shopping_product->category;
                }   
                if($google_shopping_product->instock != null){
                    $google_shopping_instock = $google_shopping_product->instock;
                }
                if($google_shopping_product->itemcondition != null){
                    $google_shopping_itemcondition = $google_shopping_product->itemcondition;
                }
            }
           
            if(!empty($google_shopping_brand) && !empty($google_shopping_category) && !empty($google_shopping_instock) && !empty($google_shopping_itemcondition)){
                $data .= "\n"."p".$value->id."\t".$value->name."\t".$description."\t".$url."\t".$google_shopping_itemcondition."\t".$price." VND"."\t".$google_shopping_instock."\t".$value->image."\t".$google_shopping_brand."\t".$google_shopping_category;
            }
            unset($google_shopping_brand);
            unset($google_shopping_category);
            unset($google_shopping_instock);
            unset($google_shopping_itemcondition);
        }

       

        Storage::disk('local')->put('products.txt', "\xEF\xBB\xBF" . $data);
        echo "Tạo datafeeds thành công ! <a href='http://localhost:20123/uploads/products.txt'>Link file datafeeds<a/>";

    }
}
