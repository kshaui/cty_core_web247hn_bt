<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
use Cache;
class LocationController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Tỉnh - Thành Phố';
        $this->table_name = 'location';
        parent::__construct();
    }
    public function index(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name,'id');
        $listdata->add('name','Tên Tỉnh / Thành Phố','string',1);
        $listdata->add('','Thông tin','string',0);
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        // $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }
    public function create()
    {
        //
        $this->checkRole($this->table_name.'_create');

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tỉnh / Thành Phố');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa Tỉnh / Thành Phố');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        $data_insert = compact('name','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }
    public function show($id)
    {
        //
        
    }
    public function edit($id)
    {
        //
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tỉnh / Thành phố');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập Tỉnh - Thành phố');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('name','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        
        $old = [
            'name'=>$data_edit->name,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }
    public function destroy($id)
    {
        
    }
}
