<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
use Cache;

class AddressController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Địa chỉ';
        $this->table_name = 'address';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_access');
        $location_array = [];
        $location = DB::table('location')->where('status',1)->get();
        if($location->count()){
            foreach($location as $key => $value){
                $location_array[$value->id] = $value->name;
            }
        }
        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên cửa hàng','string',1);
        $listdata->add('location_id','Tỉnh / Thành phố');
        $listdata->add('address','Địa chỉ','string',1);
        $listdata->add('mobile','ĐT di động');
        $listdata->add('phone','ĐT bàn');
        $listdata->add('','Thông tin');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','location_array'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $location_array = [];
        $location = DB::table('location')->where('status',1)->get();
        if($location->count()){
            foreach($location as $key => $value){
                $location_array[$value->id] = $value->name;
            }
        }
        $form = new MyForm();
        $data_form[] = $form->select('location_id','',1,'Tỉnh / Thành phố',$location_array);
        $data_form[] = $form->text('name','',1,'Tên cửa hàng');
        $data_form[] = $form->text('address','',0,'Địa chỉ');
        $data_form[] = $form->text('mobile','',0,'ĐT di động');
        $data_form[] = $form->text('phone','',0,'ĐT bàn');
        $data_form[] = $form->textarea('map','',0,'Link GG MAP');
        $data_form[] = $form->textarea('work_time','',0,'Giờ làm việc');
        
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');

        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');
        $this->validate_form($request,'name',1,'Bạn chưa nhập tên cửa hàng');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $status = 1;
        $data_form = $request->all();
        extract($data_form,EXTR_OVERWRITE);
        $data_insert = compact('name','location_id','address','mobile','phone','status','map','work_time','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);

        Cache::forget('address');
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $location_array = [];
        $location = DB::table('location')->where('status',1)->get();
        if($location->count()){
            foreach($location as $key => $value){
                $location_array[$value->id] = $value->name;
            }
        }
        $this->checkRole($this->table_name.'_edit');
        $address = DB::table('address')->find($id);
        $form = new MyForm();
        $data_form[] = $form->select('location_id',$address->location_id,1,'Tỉnh / Thành Phô',$location_array);
        $data_form[] = $form->text('name',$address->name,1,'Tên cửa hàng');
        $data_form[] = $form->text('address',$address->address,0,'Địa chỉ');
        $data_form[] = $form->text('mobile',$address->mobile,0,'ĐT di động');
        $data_form[] = $form->text('phone',$address->phone,0,'ĐT bàn');
        $data_form[] = $form->textarea('map',$address->map,0,'Link GG MAP');
        $data_form[] = $form->textarea('work_time',$address->work_time,0,'Giờ làm việc');
        $data_form[] = $form->checkbox('status',$address->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tên cửa hàng');
        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('name','location_id','address','phone','mobile','map','work_time','status','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        $old = [
            'name'=>$data_edit->name,
            'location_id'=>$data_edit->location_id,
            'address' => $data_edit->address,
            'phone'=>$data_edit->phone,
            'mobile'=>$data_edit->mobile,
            'map'=>$data_edit->map,
            'work_time'=>$data_edit->work_time,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        
        Cache::forget('address');
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
