<?php
namespace App\Modules\MailConfig\Customs;

use Illuminate\Mail\TransportManager;

class FormTransportManager extends TransportManager {

    /**
     * Create a new manager instance.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;

        $this->app['config']['mail'] = [
            'driver'        => config('mail.driver'),
            'host'          => config('mail.host'),
            'port'          => config('mail.port'),
            'from'          => [
                'address'   => config('mail.from.address'),
                'name'      => config('mail.from.name'),
            ],
            'encryption'    => config('mail.encryption'),
            'username'      => config('mail.username'),
            'password'      => config('mail.password'),
            'sendmail'      => config('mail.sendmail'),
        ];
    }
}