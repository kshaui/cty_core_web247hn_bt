<?php
namespace App\Modules\MailConfig\Customs;

use Illuminate\Mail\TransportManager;
use DB;
use Cache;
class CustomTransportManager extends TransportManager {

    /**
     * Create a new manager instance.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;

        $option = Cache::remember('setting_mail_configs', 300, function() {
            return DB::table('options')->select('value')->where('name','mail_configs')->first();
        });
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }

        $this->app['config']['mail'] = [
            'driver'        => $data['driver']??config('mail.driver'),
            'host'          => $data['host']??config('mail.host'),
            'port'          => $data['port']??config('mail.port'),
            'from'          => [
                'address'   => $data['from_address']??config('mail.from.address'),
                'name'      => $data['from_name']??config('mail.from.name'),
            ],
            'encryption'    => $data['encryption']??config('mail.encryption'),
            'username'      => $data['username']??config('mail.username'),
            'password'      => $data['password']??config('mail.password'),
            'sendmail'      => $data['sendmail']??config('mail.sendmail'),
        ];
    }
}