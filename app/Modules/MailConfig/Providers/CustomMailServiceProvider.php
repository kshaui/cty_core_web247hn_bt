<?php
namespace App\Modules\MailConfig\Providers;

use Illuminate\Mail\MailServiceProvider;
use App\Modules\MailConfig\Customs\CustomTransportManager;

class CustomMailServiceProvider extends MailServiceProvider{

    protected function registerSwiftTransport(){
        $this->app->singleton('swift.transport', function($app) {
            return new CustomTransportManager($app);
        });
    }
}