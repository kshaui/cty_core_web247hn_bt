<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function getUrl() {
    	return route('web.pages.show',$this->slug);
    }
    public function getName()
    {
        return $this->name;
    }
    public function getDes($size)
    {
        return cutString(removeHTML($this->detail),$sze);
    }
}
