@php
  dd('ewhjdfkwekfwerkfgbjwerkvbjebwjkgbgk');
@endphp
<header class="headerMob">
	<div class="container">
		<div class="headerMob-top">
			<div class="headerMob-top__btn">
				<span class="toggle">
					<a href="javascrip:;" class="button" id="compare-open">
						<span style="" onclick=""></span>
					</a>
				</span>
				<div id="main-nav" class="sidenav">
					<ul class="first-nav">
						<li data-nav-close="false" ></li>
						<li class="cryptocurrency">
							@php
								$menu_header1 = json_decode($config_general['menu_header'], true, 5) ;
								$menu_header = json_decode($config_general['menu_header1'], true, 5) ;
							@endphp

							@if(isset($menu_header1) && $menu_header1 !='')
							@foreach($menu_header1 as $value)
							<li>
								<a href="{{$value['link']??''}}">{{$value['name']??''}} 
								</a>
								<?php
								$menu_child = $value['children']??'';
								?>
								@if(isset($menu_child) && $menu_child !='')
								<ul class="submenu">
									@foreach($menu_child as $v)
									<li><a href="{{$v['link']??''}}">{{$v['name']??''}}</a></li>
									@endforeach
								</ul>
								@endif  
							</li>
							@endforeach
							@endif

							@if(isset($menu_header) && $menu_header !='')
							@foreach($menu_header as $value)
							<li>
								<a href="{{$value['link']??''}}">{{$value['name']??''}} 
								</a>
								<?php
								$menu_child = $value['children']??'';
								?>
								@if(isset($menu_child) && $menu_child !='')
								<ul class="submenu">
									@foreach($menu_child as $v)
									<li><a href="{{$v['link']??''}}">{{$v['name']??''}}</a></li>
									@endforeach
								</ul>
								@endif  
							</li>
							@endforeach
							@endif

						</li>
					</ul>
					
				</div>

			   	<script>
			   		(function($) {
			   			var $main_nav = $('#main-nav');
			   			var $toggle = $('.toggle');

			   			var defaultData = {
			   				maxWidth: false,
			   				customToggle: $toggle,
			   				navTitle: '',
			   				levelTitles: true,
			   				pushContent: '#container',
			   				insertClose: 2,
			   				closeLevels: false
			   			};

          			// add new items to original nav
          			$main_nav.find('li.add').children('a').on('click', function() {
          				var $this = $(this);
          				var $li = $this.parent();
          				var items = eval('(' + $this.attr('data-add') + ')');

          				$li.before('<li class="new"><a>'+items[0]+'</a></li>');

          				items.shift();

          				if (!items.length) {
          					$li.remove();
          				}
          				else {
          					$this.attr('data-add', JSON.stringify(items));
          				}

          				Nav.update(true);
          			});

			          // call our plugin
			          var Nav = $main_nav.hcOffcanvasNav(defaultData);

			          // demo settings update

			          const update = (settings) => {
			          	if (Nav.isOpen()) {
			          		Nav.on('close.once', function() {
			          			Nav.update(settings);
			          			Nav.open();
			          		});

			          		Nav.close();
			          	}
			          	else {
			          		Nav.update(settings);
			          	}
			          };

			          $('.actions').find('a').on('click', function(e) {
			          	e.preventDefault();

			          	var $this = $(this).addClass('active');
			          	var $siblings = $this.parent().siblings().children('a').removeClass('active');
			          	var settings = eval('(' + $this.data('demo') + ')');

			          	update(settings);
			          });

			          $('.actions').find('input').on('change', function() {
			          	var $this = $(this);
			          	var settings = eval('(' + $this.data('demo') + ')');

			          	if ($this.is(':checked')) {
			          		update(settings);
			          	}
			          	else {
			          		var removeData = {};
			          		$.each(settings, function(index, value) {
			          			removeData[index] = false;
			          		});

			          		update(removeData);
			          	}
			          });
			      })(jQuery);
			  	</script>
			</div>

			<div class="headerMob-top__logo">
				<a href="/">
					<img src="/assets/img/logo.jpg">
				</a>
			</div>
			<div class="headerMob-top__cart">
				<a href="{{ route('cart.show') }}">
					<button>
						<i class="fa fa-cart-plus" aria-hidden="true"></i>
						<span id="counts"><?php echo Cart::count(); ?></span>
					</button>
				</a>
			</div>
		</div>
		<div class="clear" style="clear: both;"></div>
		<div class="headerMob-bottom">
			<form action="{{ route('web.search') }}" method="get" id="searchform" role="search">
				{!! csrf_field() !!}
				<input type="text" class="search_inp" autocomplete="off" name="search" placeholder=" Bạn cần mua gì?">
				<button><i class="fa fa-search" aria-hidden="true"></i></button>
				<div class="suggest">
					@php
					$menu_search = json_decode($config_general['menu_search'], true, 5) ;
					@endphp
					<ul class="suggest-search">
						@foreach($menu_search as $value)
						<li><a href="{{$value['link']??''}}">{{$value['name']??''}}</a></li>
						@endforeach
					</ul>
				</div>
			</form>
		</div>
	</div>
</header>