@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org",
            "@type": "Store",
            "image": "{{config('app.url')}}/public/assets/img/logo.png",
            "name": "{{config('app.name')}}",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "120 Thái Hà",
                "addressLocality": "Q. Đống Đa",
                "addressRegion": "Hà Nội",
                "postalCode": "100000",
                "addressCountry": "VN"
            },
            "priceRange": "$$",
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": 21.011915,
                "longitude": 105.821283
            },
            "telephone": "0969.120.120 - 0433.120.120"

        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>
@endsection
@section('content')
   mobile
@endsection