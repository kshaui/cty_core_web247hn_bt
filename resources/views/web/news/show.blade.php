@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org",
                "@type": "NewsArticle",
                "mainEntityOfPage": {
                    "@type": "WebPage",
                    "@id": "{{$news->getUrl()}}"
                },
                "headline": "{{$news->name}}",
                "image": {
                    "@type": "ImageObject",
                    "url": "{{$news->getImage()}}",
                    "height": 800,
                    "width": 800
                },
                // "aggregateRating": {
                //     "@type": "AggregateRating",
	               //  "ratingValue": "{{@$rank_peren}}",
	               //  "reviewCount": "{{@$rank_count}}"
                // },
                "datePublished": "{{date('Y/m/d',strtotime($news->created_at))}}",
                "dateModified": "{{date('Y/m/d',strtotime($news->updated_at))}}",
                "author": {
                    "@type": "Person",
                    "name": "{{$admin_name??'web247hn'}}"
                },
                "publisher": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}",
                    "logo": {
                        "@type": "ImageObject",
                        "url": "https://web247hn.com/themes/img/logo.png",
                        "width": 600,
                        "height": 60
                    }
                },
                "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($news->detail),150) : $meta_seo['description']}}"
            }, {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "{{config('app.name')}}",
                "url": "{{config('app.url')}}"
            },
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "url": "{{config('app.url')}}",
                "@id": "{{config('app.url')}}/#organization",
                "name": "web247hn",
                "logo": "{{config('app.url')}}/public/assets/img/logo.png"
            }
        ]
    }




</script>
@endsection
@section('content')
    Welcome !
@endsection