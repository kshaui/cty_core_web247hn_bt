@php
$product = DB::table('products')->where('status',1)->where('id',$value->type_id)->first();
$count_rating = round($value->value,1);
@endphp
<td>
@if(isset($product->slug))

    <strong><a href="{{ route('web.products.show',$product->slug) }}" target="_blank">{{ $product->name }}</a></strong>
    @endif
</td>
<td>
    <div class="rate">
        @for($i = 0; $i < 5; $i ++)
            @if($i < $count_rating && $i + 0.5 == $count_rating)
                <i style="color: #ffb933" class="fa fa-star-half-o vote "
                   aria-hidden="true"></i>
            @elseif($i < $count_rating)
                <i style="color: #ffb933" class='fa fa-star vote'
                ></i>
            @else
                <i style="color: #adadad" class='fa fa-star'></i>
            @endif
        @endfor
    </div>

</td>
{{-- <td>{{ $value->host }}</td> --}}
<td> {!! change_time_to_text($value->created_at) !!}</td>
{{-- <td class="center" style="position: relative;width: 80px;">
    <input style="width: 20px;height: 16px;" type="checkbox" class="show-sale-{!! $value->id !!}"
           @if($value->status ==1) checked="checked" @endif value="{!!$value->status!!}" class="form-control quick-edit"
           onchange="show_sale({!!$value->id!!},'ratings')">
    <img class="img-show-sale-{!! $value->id !!}"
         style="width: 20px;height: 20px;position: absolute;top: 10px;left: 29px;display: none;"
         src="/images/loading2.gif" alt="">
</td> --}}