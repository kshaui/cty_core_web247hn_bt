<div class="alert alert-info" data-toggle="collapse" data-target="#{{$name}}">
	<strong>{{__($title??'')}}</strong>
</div>
<div id="{{$name}}" class="collapse" style="height: auto;">
	
	@include('admin.layouts.form.table.1_image_2_input',[
		'full' => 'false',
		'name' => $name,
		'slug' => $name,
		'value' => $data[$name]??'',
		'label' => 'Danh sách thương hiệu',
		'placeholder_input_1' => 'Link thương hiệu',
		'placeholder_input_2' => 'Tên thương hiệu',
	])
	
	@php $banner_text = 'banner_text'; @endphp
	
</div>