<div class="alert alert-info" data-toggle="collapse" data-target="#{{$name}}">
	<strong>{{__($title??'')}}</strong>
</div>
<div id="{{$name}}" class="collapse" style="height: auto;">
	
	@include('admin.layouts.form.table.1_input_1_text',[
		'full' => 'false',
		'name' => $name,
		'slug' => $name,
		'value' => $data[$name]??'',
		'label' => 'Danh sách short code',
		'placeholder_input_1' => 'Tên short code',
		'placeholder_input_2' => 'Nội dung',
	])
	
	@php $banner_text = 'banner_text'; @endphp
	
</div>