{{-- 
	@include('admin.layouts.form.multi_cate',[
		'name' => 'text',
		'value' => [],
		'title' => '',
		'required' => 1,
		'options' => [],
	])
 --}}
<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<div id="{!! $name??'' !!}" class="multi-cate-wrap multi-check">
		@foreach($options as $k => $v)
			@php
				$str_level = '';
				for($i=0;$i<$v['level'];$i++) {
					$str_level .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
			@endphp
			<label>{!! $str_level !!}<input type="checkbox" name="{!! $name??'' !!}[]" value="{{ $v['id'] }}" @if(in_array($v['id'],$value??[])) checked="checked" @endif > {{ $v['name'] }}</label><br>
		@endforeach()
		</div>
	</div>
</div>