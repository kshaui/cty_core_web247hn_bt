{{-- 
	@include('admin.layouts.form.slug',[
		'name' => 'text',
		'value' => 'text',
		'title' => '',
	])
 --}}
<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick">* </span> {!! $title??'' !!}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<input class="form-control slug-field" type="text" name="{!! $name??'' !!}" id="{!! $name??'' !!}" value="{!! $value??'' !!}">
	</div>
</div>