{{-- 
	@include('admin.layouts.form.image',[
		'name' => 'text',
		'value' => 'text',
		'title' => 'text',
		'required' => 1,
		'title_btn' => 'text',
		'helper_text' => 'text',
	])
 --}}
<div class="form-group">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12">
        <a class="btn btn-primary" href="javascript:;" onclick="media_popup('add','single','{!!$name??''!!}','{!! $title_btn??'' !!}');">{!! $title??'' !!}</a>
        <div class="clear"></div>
        <input id="{!! $name??'' !!}" type="hidden" name="{!! $name??'' !!}" value="{!!$value??''!!}">
        <img src="{!!$value??''!!}" class="thumb-img" style="width: 200px;">
        @if($helper_text != '')<p class="help-block">{!! $helper_text !!}</p>@endif
    </div>
</div>