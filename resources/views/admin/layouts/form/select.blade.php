{{-- 
	@include('admin.layouts.form.select',[
		'name' => 'text',
		'value' => 'text',
		'title' => '',
		'required' => 1,
		'disabled' => 1,
		'options' => [],
		'select2' => 1,
	])
 --}}
<div class="form-group">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12">
	    <select name="{!! $name??'' !!}" id="{!! $name??'' !!}" class="form-control{{($select2==1) ? ' select2' : ''}}">
	        @foreach($options as $k => $v)
	        	@php
					$selected = '';
					if($value==$k) {
						$selected = ' selected="selected"';
					}
					$disable = '';
					if(in_array($k,$disabled)) {
						$disable = ' disabled="disabled"';
					}
	        	@endphp
	        	<option value="{!! $k !!}"{!!$selected!!}{!!$disable!!}>{!! $v !!}</option>
	        @endforeach()
	    </select>
    </div>
 </div>