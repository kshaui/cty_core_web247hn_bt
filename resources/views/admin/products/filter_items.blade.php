@foreach($filters as $value)
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{$value->name}}</h3>
    </div>
    <div class="box-body">
    	@foreach($value->details as $v)
		@if($v->status == 1)
        <label><input type="checkbox" name="filters[]" value="{{$v->id}}" @if(isset($product_filters) && in_array($v->id,$product_filters)) checked @endif> {!!$v->name!!}</label>&nbsp;&nbsp;&nbsp;
        @endif
        @endforeach
    </div>
</div>
@endforeach