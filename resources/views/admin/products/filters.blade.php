@php
$no_filters_message = '<p class="filters-alert">Danh mục hiện tại chưa có bộ lọc. Vui lòng <a href="javascript:;" class="rechose-category">chọn lại danh mục</a> hoặc <a href="'.route("filters.create").'" target="_blank">tạo bộ lọc</a></p>';
@endphp
<div class="alert alert-info row collapsed" data-toggle="collapse" data-target="#filters" style="cursor: pointer">
	<div class="col-md-2 col-sm-2 col-xs-12 text-right"><b>Bộ lọc sản phẩm</b></div>
</div>
<div id="filters" class="row collapse">
	@if(isset($filters) && $filters)
		@include('admin.products.filter_items')
	@else 
		{!!$no_filters_message!!}
	@endif
</div>
<script type="text/javascript">
	//click nút chọn lại danh mục
	$('#filters').on('click','.rechose-category',function(){
		$('#category_id').focus();
	});
	//Khi chọn danh mục
	// $('#category_id').on('change',function(){
	// 	var category_id = $(this).val();
	// 	$.ajax({
	// 		url: '/admin/products/get-filters',
	// 		type: 'POST',
	// 		dataType: 'json',
	// 		data: {category_id: category_id},
	// 		beforeSend: function (){
	// 			console.log('Chọn lại danh mục');
	// 	    }
	// 	})
	// 	.done(function(result) {
	// 		if(result.status == 1) {
	// 			$('#filters').html(result.html);
	// 		}else if(result.status == 2){
	// 			$('#filters').html('{!!$no_filters_message!!}');
	// 		}else {
	// 			$('#filters').html(result.message);
	// 		}
	// 	})
	// 	.fail(function() {
	// 		console.log("error");
	// 	})
	// 	.always(function() {
	// 		console.log("complete");
	// 	});
	// });
</script>
<style type="text/css">
	.filters-alert {
		font-size: 1.5em;
		text-align: center;
	}
	.filters-alert a {
		color: #093;
		font-weight: bold;
	}
	#category_id:focus {
		border-color: #093;
	}
	#filters .box {
		height: auto;
		border: 1px solid #eee;
    	background-color: #fafafa;
	    margin-bottom: 20px;
	}
</style>