<?php
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'Admin\AdminController@index')->middleware('auth-admin')->name('admin.dashboard');
    Route::get('/login','Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Admin\LoginController@login')->name('admin.login.submit');
    Route::get('logout','Admin\LoginController@logout')->name('admin.logout');
    Route::get('/permission-denied','Admin\Controller@permissionDenied')->name('admin.permission.denied');
    // route cho admin user admin
    Route::group(['prefix' => 'admin_users','middleware'=>'auth-admin'], function() {
        Route::get('change-password','Admin\AdminUserController@changePassword')->name('admin.admin_user.changePassword');
        Route::post('change-password','Admin\AdminUserController@postChangePassword');
    });
    Route::resource('admin_users','Admin\AdminUserController',['middleware'=>'auth-admin']);
    //route cho settings
    Route::group(['prefix' => 'settings','middleware'=>'auth-admin'], function() {
        Route::match(['GET', 'POST'],'/general','Admin\SettingController@general')->name('admin.setting.general');
        Route::match(['GET', 'POST'],'/home','Admin\SettingController@home')->name('admin.setting.home');
        Route::match(['GET', 'POST'],'/google_shopping','Admin\SettingController@google_shopping')->name('admin.setting.google_shopping');
        Route::match(['GET', 'POST'],'/promotion','Admin\SettingController@promotion')->name('admin.setting.promotion');
        Route::match(['GET', 'POST'],'/seo','Admin\SettingController@seo')->name('admin.setting.seo');
    });
    //route cho media
    Route::group(['prefix' => 'media','middleware'=>'auth-admin'], function() {
        Route::get('library','Admin\MediaController@library')->name('admin.media.library');
        Route::post('store', 'Admin\MediaController@store')->name('admin.media.store');
        Route::post('update', 'Admin\MediaController@update')->name('admin.media.update');
        Route::post('search','Admin\MediaController@search')->name('admin.media.search');
    });
    // route cho ajax
    Route::group(['prefix' => 'ajax'], function() {
        Route::post('get-slug', 'Admin\Controller@getSlug');
        Route::post('delete-all', 'Admin\Controller@deleteAll');
        Route::post('trash-all', 'Admin\Controller@trashAll');
        Route::post('deactive-all', 'Admin\Controller@deactiveAll');
        Route::post('save-one', 'Admin\Controller@saveOne');
        Route::post('save-all', 'Admin\Controller@saveAll');
        Route::post('relate-suggest', 'Admin\Controller@relateSuggest');
        Route::post('pins', 'Admin\Controller@pins');
        Route::post('reset-password', 'Admin\AjaxController@resetPassword');

         // Duyệt ẩn bình luận admin trong reply
         Route::post('comment_success','Admin\CommentController@comment_success')->name('admin.comment_success');
         Route::post('quick_comment', 'Admin\CommentController@quick_comment');
         // lưu nhanh ko cần nhấn nút save
         Route::post('quick_edit', 'Admin\AjaxController@quick_edit')->name('admin.quick_edit');
    });
    Route::resource('system_logs','Admin\SystemLogController',['middleware'=>'auth-admin']);

    //Các module resource
    Route::resource('product_categories','Admin\ProductCategoryController',['middleware'=>'auth-admin']);
    Route::resource('products','Admin\ProductController',['middleware'=>'auth-admin']);
    Route::resource('news_categories','Admin\NewsCategoryController',['middleware'=>'auth-admin']);
    Route::resource('news','Admin\NewsController',['middleware'=>'auth-admin']);
    Route::resource('pages','Admin\PageController',['middleware'=>'auth-admin']);

    Route::resource('trademark','Admin\TrademarkController',['middleware'=>'auth-admin']);

    Route::resource('filters','Admin\FilterController',['middleware'=>'auth-admin']);
    Route::resource('filter_details','Admin\FilterDetailController',['middleware'=>'auth-admin']);

    Route::resource('partners','Admin\PartnerController',['middleware'=>'auth-admin']);

    Route::resource('comments','Admin\CommentController',['middleware'=>'auth-admin']);// đánh giá
    Route::group(['prefix' => 'comment','middleware'=>'auth-admin'], function() {
        Route::get('/{id}/reply', 'Admin\CommentController@reply')->name('comments.reply');
        Route::post('/reply/{id}', 'Admin\CommentController@postReply')->name('comments.postReply');
    });

    Route::resource('supports','Admin\SupportController',['middleware'=>'auth-admin']);// đánh giá

    Route::resource('slides','Admin\SlidesController',['middleware'=>'auth-admin']);

    Route::resource('sync_link','Admin\Sync_linkController',['middleware'=>'auth-admin']);

    Route::resource('votes','Admin\VoteController',['middleware'=>'auth-admin']);
    Route::resource('sizes','Admin\SizesController',['middleware'=>'auth-admin']);
    Route::resource('colors','Admin\ColorsController',['middleware'=>'auth-admin']);

    Route::resource('contacts','Admin\ContactController',['middleware'=>'auth-admin']);

    Route::resource('fit_categories','Admin\FitCategoryController',['middleware'=>'auth-admin']);
    Route::resource('fits','Admin\FitController',['middleware'=>'auth-admin']);

    Route::resource('location','Admin\LocationController',['middleware'=>'auth-admin']);
    Route::resource('address','Admin\AddressController',['middleware'=>'auth-admin']);
});