<?php

/*
|--------------------------------------------------------------------------
| Mobile Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Mobile routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "Mobile" middleware group. Now create something great!
|
*/
/**
 * ajax
 */

Route::group(['prefix' => 'ajax'], function() {
	Route::post('register','Mobile\AjaxController@AjaxRegister')->name('Mobile.ajax.register');
	Route::post('login','Mobile\AjaxController@AjaxLogin')->name('Mobile.ajax.login');

	/**
	 * dat hang
	 */
	Route::post('dat-hang','Mobile\AjaxController@Order')->name('Mobile.ajax.order');
});

Route::get('datafeeds','Mobile\HomeController@datafeeds');

Route::get('/', 'Mobile\HomeController@index')->name('Mobile.home');
Route::get('/test', 'Mobile\TestController@index')->name('Mobile.test');

Route::get('tim-kiem','Mobile\SearchController@index')->name('Mobile.search.index');

Route::get('page-{slug}.html','Mobile\PageController@show')->name('Mobile.pages.show');

Route::get('lien-he','Mobile\ContactController@index')->name('Mobile.contact.index');

Route::get('tin-tuc-{slug}.html','Mobile\NewsController@show')->name('Mobile.news.show');
Route::get('tin-tuc-{slug}','Mobile\NewsController@index')->name('Mobile.news_categories.show');
Route::get('tin-tuc','Mobile\NewsController@AllShow')->name('Mobile.news_categories.Allshow');

Route::get('{slug}.html','Mobile\ProductController@show')->name('Mobile.products.show');
Route::get('san-pham-{filters?}','Mobile\ProductController@AllShow')->name('Mobile.product_categories.allshow');
Route::get('san-pham-{slug}-{filters?}','Mobile\ProductController@index')->name('Mobile.product_categories.show');
