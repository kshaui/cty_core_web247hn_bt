<?php

use Illuminate\Database\Seeder;
class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $created_at = $updated_at = date('Y-m-d H:i:s');
    	$password = passwordGenerate();
        DB::table('admin_users')->insert([
            'id' => 1,
            'name' => 'dev',
            'email' => 'dev@web247hn.com',
            'password' => bcrypt($password),
            'status' => 1,
            'created_at' => $created_at,
            'updated_at' => $updated_at
        ]);
        $this->command->info('Tai khoan quan tri da duoc tao tu dong. Username: dev, Password: '.$password);
    }
}
