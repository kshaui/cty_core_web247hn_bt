<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->default(0);
            $table->string('brands')->nullable();// thương hiệu
            $table->string('sku',191)->nullable();//mã sp
            $table->string('name',191);
            $table->string('slug',191);
            $table->integer('price')->nullable()->default(0);
            $table->integer('price_old')->nullable()->default(0);
            $table->integer('import_price')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('size')->nullable();
            $table->integer('amount')->nullable();

            $table->tinyInteger('instock_status')->nullable()->default(1);

            $table->string('warranty')->nullable();
            $table->string('image',191)->nullable();
            $table->text('slides')->nullable();
            $table->text('videos')->nullable();
            $table->longText('parameter')->nullable();// thông số
            $table->text('promotion')->nullable();
            $table->text('description')->nullable();
            $table->longText('detail')->nullable();
            $table->text('date_start_sale')->nullable();
            $table->text('date_stop_sale')->nullable();
            $table->tinyInteger('warehouse_status')->default(1);//array(1=>'Còn hàng',2=>'Hết hàng',3=>'Sắp về',4=>'Sắp ra mắt',5=>'Ngừng kinh doanh');
            $table->string('related_products',191)->nullable();
            $table->string('related_news',191)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->unique('id','id_UNIQUE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
